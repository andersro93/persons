using System.Collections.Generic;
using BekkK8s.Persons.Core.Entities;

namespace BekkK8s.Persons.Core.Generators
{
    public interface IPersonGenerator
    {
        public IEnumerable<IPerson> GetCollection(int count);
    }
}