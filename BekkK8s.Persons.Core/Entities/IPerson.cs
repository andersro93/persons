using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BekkK8s.Persons.Core.Entities
{
    public interface IPerson
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        
        [JsonPropertyName("ssn")]
        public string Ssn { get; set; }
        
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }
        
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }
        
        [JsonPropertyName("dateOfBirth")]
        public DateTime DateOfBirth { get; set; }
        
        [JsonPropertyName("avatarUrl")]
        public string AvatarUrl { get; set; }
        
        [JsonPropertyName("phoneNumber")]
        public string PhoneNumber { get; set; }
        
        [JsonPropertyName("email")]
        public string Email { get; set; }
        
        [JsonPropertyName("addresses")]
        public IEnumerable<IAddress> Addresses { get; set; }
    }
}