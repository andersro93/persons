using System;
using System.Collections.Generic;

namespace BekkK8s.Persons.Core.Entities
{
    public class Person : IPerson
    {
        public Guid Id { get; set; }
        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string AvatarUrl { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public IEnumerable<IAddress> Addresses { get; set; }
    }
}