namespace BekkK8s.Persons.Core.Migrators
{
    public interface IMigrator
    {
        public void MigrateDatabase();
        public void SeedDatabase();
    }
}