using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BekkK8s.Persons.Core.Entities;

namespace BekkK8s.Persons.Core.Repositories
{
    public interface IPersonRepository
    {
        public Task<IEnumerable<IPerson>> GetAll();
        public Task<IPerson> FindById(Guid id);
    }
}