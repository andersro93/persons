using BekkK8s.Persons.Core.Migrators;
using BekkK8s.Persons.Data.Generators.Infrastructure;
using BekkK8s.Persons.Data.Infrastructure;
using BekkK8s.Persons.MockData.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BekkK8s.Persons.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (Environment.IsStaging())
            {
                services.AddBekkK8SMockData();
            }
            else
            {
                services.AddBekkK8PersonsServices(Configuration.GetConnectionString("Persons"));
            }
            
            services.AddBekkK8SDataGenerators();
            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMigrator migrator)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            migrator.MigrateDatabase();
            migrator.SeedDatabase();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}