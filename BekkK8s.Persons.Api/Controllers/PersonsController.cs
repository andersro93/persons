using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BekkK8s.Persons.Api.Dtos.Persons;
using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace BekkK8s.Persons.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonsController : ControllerBase
    {
        private readonly IPersonRepository _personRepository;

        public PersonsController(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }
        
        [HttpGet]
        public async Task<PersonsListResponse> Index()
        {
            // Retrieve the persons from Repository
            IEnumerable<IPerson> persons = await _personRepository.GetAll();
            
            return new PersonsListResponse(persons);
        }

        [HttpGet("{id}")]
        public async Task<IPerson> Get(Guid id)
        {
            // Retrieve the person from the repository
            IPerson person = await _personRepository.FindById(id);
            
            return person;
        }
    }
}