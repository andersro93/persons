using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using BekkK8s.Persons.Core.Entities;

namespace BekkK8s.Persons.Api.Dtos.Persons
{
    public class PersonsListResponse
    {
        public PersonsListResponse(IEnumerable<IPerson> persons)
        {
            Persons = persons;
            TotalPersons = persons.Count();
        }
        
        [JsonPropertyName("persons")] 
        public IEnumerable<IPerson> Persons { get; set; }
        
        [JsonPropertyName("totalPersons")]
        public int TotalPersons { get; set; }
    }
}