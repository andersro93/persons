using System;
using System.Collections.Concurrent;
using BekkK8s.Persons.Core.Entities;

namespace BekkK8s.Persons.Data.Database
{
    public class FakeDatabase
    {
        public readonly ConcurrentDictionary<Guid, IPerson> Database = new ConcurrentDictionary<Guid, IPerson>();
    }
}