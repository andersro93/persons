using BekkK8s.Persons.Core.Migrators;
using BekkK8s.Persons.Core.Repositories;
using BekkK8s.Persons.Data.Database;
using BekkK8s.Persons.MockData.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace BekkK8s.Persons.MockData.Infrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddBekkK8SMockData(this IServiceCollection services) => services
            .AddSingleton<FakeDatabase>()
            .AddTransient<IMigrator, Migrator>()
            .AddTransient<IPersonRepository, FakePersonRepository>();
    }
}