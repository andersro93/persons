using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Generators;
using BekkK8s.Persons.Core.Migrators;
using BekkK8s.Persons.Data.Database;

namespace BekkK8s.Persons.MockData.Infrastructure
{
    public class Migrator : IMigrator
    {
        private readonly FakeDatabase _fakeDatabase;
        private readonly IPersonGenerator _personGenerator;

        public Migrator(FakeDatabase fakeDatabase, IPersonGenerator personGenerator)
        {
            _fakeDatabase = fakeDatabase;
            _personGenerator = personGenerator;
        }
        
        public void MigrateDatabase() {}

        public void SeedDatabase()
        {
            foreach (IPerson person in _personGenerator.GetCollection(500))
            {
                _fakeDatabase.Database.TryAdd(person.Id, person);
            }
        }
    }
}