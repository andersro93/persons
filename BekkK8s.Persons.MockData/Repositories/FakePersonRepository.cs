using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Repositories;
using BekkK8s.Persons.Data.Database;

namespace BekkK8s.Persons.MockData.Repositories
{
    public class FakePersonRepository : IPersonRepository
    {
        private readonly FakeDatabase _fakeDatabase;

        public FakePersonRepository(FakeDatabase fakeDatabase)
        {
            _fakeDatabase = fakeDatabase;
        }

        public async Task<IEnumerable<IPerson>> GetAll() => await Task.FromResult(_fakeDatabase.Database.Values);

        public async Task<IPerson> FindById(Guid id)
        {
            _fakeDatabase.Database.TryGetValue(id, out IPerson person);
            return await Task.FromResult(person);
        }
    }
}