FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /build

# Copy the projects files
COPY BekkK8s.Persons.sln ./BekkK8s.Persons.sln
COPY BekkK8s.Persons.Api/BekkK8s.Persons.Api.csproj ./BekkK8s.Persons.Api/BekkK8s.Persons.Api.csproj
COPY BekkK8s.Persons.Core/BekkK8s.Persons.Core.csproj ./BekkK8s.Persons.Core/BekkK8s.Persons.Core.csproj
COPY BekkK8s.Persons.Data/BekkK8s.Persons.Data.csproj ./BekkK8s.Persons.Data/BekkK8s.Persons.Data.csproj
COPY BekkK8s.Persons.MockData/BekkK8s.Persons.MockData.csproj ./BekkK8s.Persons.MockData/BekkK8s.Persons.MockData.csproj
COPY BekkK8s.Persons.Data.Generators/BekkK8s.Persons.Data.Generators.csproj ./BekkK8s.Persons.Data.Generators/BekkK8s.Persons.Data.Generators.csproj
COPY BekkK8s.Persons.Tests.Unit/BekkK8s.Persons.Tests.Unit.csproj ./BekkK8s.Persons.Tests.Unit/BekkK8s.Persons.Tests.Unit.csproj
COPY BekkK8s.Persons.Tests.Integration/BekkK8s.Persons.Tests.Integration.csproj ./BekkK8s.Persons.Tests.Integration/BekkK8s.Persons.Tests.Integration.csproj

RUN ["dotnet", "restore"]

COPY . ./

RUN ["dotnet", "publish", "-c", "Release", "-o", "out"]

## RUNTIME ##
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 as runtime
WORKDIR /app

# Copy all files from build
COPY --from=build /build/out .

# Tell docker that we expose port 80
EXPOSE 80

# Set the default run command
ENTRYPOINT ["dotnet", "BekkK8s.Persons.Api.dll"]

