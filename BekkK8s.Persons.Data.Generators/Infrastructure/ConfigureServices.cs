using BekkK8s.Persons.Core.Generators;
using BekkK8s.Persons.Data.Seeder.Generators;
using Microsoft.Extensions.DependencyInjection;

namespace BekkK8s.Persons.Data.Generators.Infrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddBekkK8SDataGenerators(this IServiceCollection services)
        {
            services.AddTransient<IPersonGenerator, PersonGenerator>();

            return services;
        }
    }
}