using System;
using System.Collections.Generic;
using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Generators;
using Bogus;
using Bogus.Extensions.Norway;
using Person = BekkK8s.Persons.Core.Entities.Person;

namespace BekkK8s.Persons.Data.Seeder.Generators
{
    public class PersonGenerator : IPersonGenerator
    {
        private const string Locale = "nb_NO";
        private const int Seed = 1337;
        private readonly Faker<Person> _personGenerator;
        private readonly Faker<Address> _addressGenerator;

        public PersonGenerator()
        {
            Randomizer.Seed = new Random(Seed);
            
            _addressGenerator = new Faker<Address>(Locale)
                .RuleFor(p => p.Id, faker => faker.Random.Guid())
                .RuleFor(a => a.StreetName, faker => faker.Address.StreetAddress())
                .RuleFor(a => a.ZipCode, faker => faker.Address.ZipCode())
                .RuleFor(a => a.City, faker => faker.Address.City())
                .RuleFor(a => a.Country, faker => faker.Address.Country())
                .RuleFor(a => a.Longitude, faker => faker.Address.Longitude())
                .RuleFor(a => a.Latitude, faker => faker.Address.Latitude());

            _personGenerator = new Faker<Person>(Locale)
                .RuleFor(p => p.Id, faker => faker.Random.Guid())
                .RuleFor(p => p.Ssn, faker => faker.Person.Fødselsnummer())
                .RuleFor(p => p.FirstName, faker => faker.Person.FirstName)
                .RuleFor(p => p.LastName, faker => faker.Person.LastName)
                .RuleFor(p => p.DateOfBirth, faker => faker.Person.DateOfBirth)
                .RuleFor(p => p.AvatarUrl, faker => faker.Person.Avatar)
                .RuleFor(p => p.PhoneNumber, faker => faker.Person.Phone)
                .RuleFor(p => p.Email, faker => faker.Person.Email)
                .RuleFor(p => p.Addresses, faker => _addressGenerator.Generate(2).ToArray());
            
        }

        public IEnumerable<IPerson> GetCollection(int count) => _personGenerator.Generate(count);
    }
}