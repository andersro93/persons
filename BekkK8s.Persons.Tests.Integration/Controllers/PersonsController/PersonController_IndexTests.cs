using System.Collections.Generic;
using System.Threading.Tasks;
using BekkK8s.Persons.Api.Dtos.Persons;
using BekkK8s.Persons.Core.Entities;
using FakeItEasy;
using NUnit.Framework;

namespace BekkK8s.Persons.Tests.Integration.Controllers.PersonsController
{
    [TestFixture]
    public class PersonController_IndexTests : BasePersonsControllerTests
    {
        [Test]
        public async Task TheIndexActionReturnsAPersonsListResponse()
        {
            // Arrange
            A.CallTo(() => PersonRepository.GetAll()).Returns(new List<IPerson>());
            
            // Act
            var result = await PersonsController.Index();
            
            // Assert
            Assert.IsInstanceOf<PersonsListResponse>(result);
        }

        [Test]
        public async Task TheIndexActionReturnsTheCorrectNumberOfPersons()
        {
            // Arrange
            int numberOfResults = 15;
            A.CallTo(() => PersonRepository.GetAll()).Returns(A.CollectionOfFake<IPerson>(numberOfResults));
            
            // Act
            var result = await PersonsController.Index();
            
            // Assert
            Assert.AreEqual(result.TotalPersons, numberOfResults);
        }
    }
}