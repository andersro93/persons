using BekkK8s.Persons.Core.Repositories;
using FakeItEasy;
using NUnit.Framework;

namespace BekkK8s.Persons.Tests.Integration.Controllers.PersonsController
{
    public abstract class BasePersonsControllerTests
    {
        protected Api.Controllers.PersonsController PersonsController;
        protected IPersonRepository PersonRepository;

        [SetUp]
        public void SetUp()
        {
            PersonRepository = A.Fake<IPersonRepository>();
            
            PersonsController = new Api.Controllers.PersonsController(PersonRepository);
        }
    }
}