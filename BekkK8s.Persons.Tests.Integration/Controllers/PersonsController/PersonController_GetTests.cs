using System;
using System.Threading.Tasks;
using BekkK8s.Persons.Core.Entities;
using FakeItEasy;
using NUnit.Framework;

namespace BekkK8s.Persons.Tests.Integration.Controllers.PersonsController
{
    [TestFixture]
    public class PersonController_GetTests : BasePersonsControllerTests
    {
        [Test]
        public async Task TheGetActionReturnsTheCorrectPerson()
        {
            // Arrange
            Guid fakePersonId = Guid.NewGuid();
            IPerson fakePerson = A.Fake<IPerson>();
            fakePerson.Id = fakePersonId;
            
            A.CallTo(() => PersonRepository.FindById(A<Guid>._)).Returns(fakePerson);
            
            // Act
            var result = await PersonsController.Get(fakePersonId);
            
            // Assert
            Assert.AreEqual(result, fakePerson);
        }
    }
}