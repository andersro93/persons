using BekkK8s.Persons.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace BekkK8s.Persons.Data.Contexts
{
    public class PersonsDbContext : DbContext
    {
        public PersonsDbContext(DbContextOptions<PersonsDbContext> dbContextOptions) : base(dbContextOptions) {}
        
        public DbSet<PersonEntity> PersonEntities { get; set; }
        public DbSet<AddressEntity> AddressEntities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddressEntity>()
                .HasOne<PersonEntity>(a => a.PersonEntity)
                .WithMany(p => p.Addresses)
                .HasForeignKey(a => a.PersonId);
        }
    }
}