using System;
using System.ComponentModel.DataAnnotations;

namespace BekkK8s.Persons.Data.Entities
{
    public class AddressEntity
    {
        [Key] 
        public Guid Id { get; set; } = Guid.NewGuid();
        
        [Required]
        public Guid PersonId { get; set; }
        public PersonEntity PersonEntity { get; set; }
        
        public string StreetName { get; set; }
        
        public string ZipCode { get; set; }
        
        public string City { get; set; }
        
        public string Country { get; set; }
        
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
    }
}