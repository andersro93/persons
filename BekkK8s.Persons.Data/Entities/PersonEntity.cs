using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BekkK8s.Persons.Data.Entities
{
    public class PersonEntity
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public string Ssn { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public DateTime DateOfBirth { get; set; }
        
        public string AvatarUrl { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string Email { get; set; }
        
        public ICollection<AddressEntity> Addresses { get; set; }
    }
}