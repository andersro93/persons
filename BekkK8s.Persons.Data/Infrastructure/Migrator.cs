using System.Collections.Generic;
using System.Linq;
using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Generators;
using BekkK8s.Persons.Core.Migrators;
using BekkK8s.Persons.Data.Contexts;
using BekkK8s.Persons.Data.Entities;

namespace BekkK8s.Persons.Data.Infrastructure
{
    public class Migrator : IMigrator
    {
        private readonly PersonsDbContext _personsDbContext;
        private readonly IPersonGenerator _personGenerator;

        public Migrator(PersonsDbContext personsDbContext, IPersonGenerator personGenerator)
        {
            _personsDbContext = personsDbContext;
            _personGenerator = personGenerator;
        }

        public void MigrateDatabase()
        {
            // Never do this in any systems. Here we are doing it the easy way for demo purposes ;)
            _personsDbContext.Database.EnsureDeleted();
            _personsDbContext.Database.EnsureCreated();
        }

        public void SeedDatabase()
        {
            IEnumerable<IPerson> persons = _personGenerator.GetCollection(500);
            
            _personsDbContext.PersonEntities.AddRange(persons.Select(p => new PersonEntity()
            {
                Id = p.Id,
                Ssn = p.Ssn,
                FirstName = p.FirstName,
                LastName = p.LastName,
                Email = p.Email,
                PhoneNumber = p.PhoneNumber,
                AvatarUrl = p.AvatarUrl,
                DateOfBirth = p.DateOfBirth,
                Addresses = p.Addresses.Select(a => new AddressEntity()
                {
                    Id = a.Id,
                    StreetName = a.StreetName,
                    City = a.City,
                    ZipCode = a.ZipCode,
                    Country = a.Country,
                    Latitude = a.Latitude,
                    Longitude = a.Longitude,
                }).ToList()
            }).ToList());

            _personsDbContext.SaveChanges();
        }
    }
}