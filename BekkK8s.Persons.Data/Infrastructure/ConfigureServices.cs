using BekkK8s.Persons.Core.Migrators;
using BekkK8s.Persons.Core.Repositories;
using BekkK8s.Persons.Data.Contexts;
using BekkK8s.Persons.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BekkK8s.Persons.Data.Infrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddBekkK8PersonsServices(this IServiceCollection services,
            string connectionString)
        {
            // Add the db context
            services.AddDbContext<PersonsDbContext>(options => options.UseNpgsql(connectionString));

            // Repositories
            services.AddTransient<IPersonRepository, PersonRepository>();

            // Migrator helper
            services.AddTransient<IMigrator, Migrator>();
            
            return services;
        }
    }
}