using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BekkK8s.Persons.Core.Entities;
using BekkK8s.Persons.Core.Repositories;
using BekkK8s.Persons.Data.Contexts;
using BekkK8s.Persons.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace BekkK8s.Persons.Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly PersonsDbContext _personsDbContext;

        public PersonRepository(PersonsDbContext personsDbContext)
        {
            _personsDbContext = personsDbContext;
        }

        public async Task<IEnumerable<IPerson>> GetAll() => (await _personsDbContext.PersonEntities
                .ToListAsync())
            .Select(person => new Person()
            {
                Id = person.Id,
                Ssn = person.Ssn,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Email = person.Email,
                PhoneNumber = person.PhoneNumber,
                AvatarUrl = person.AvatarUrl,
                DateOfBirth = person.DateOfBirth,
            }).ToList();

        public async Task<IPerson> FindById(Guid id)
        {
            PersonEntity person = await _personsDbContext.PersonEntities
                .Include(p => p.Addresses)
                .Where(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (person == null)
            {
                return null;
            }

            return new Person()
            {
                Id = person.Id,
                Ssn = person.Ssn,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Email = person.Email,
                PhoneNumber = person.PhoneNumber,
                AvatarUrl = person.AvatarUrl,
                DateOfBirth = person.DateOfBirth,
                Addresses = person.Addresses.Select(a => new Address()
                {
                    StreetName = a.StreetName,
                    ZipCode = a.ZipCode,
                    City = a.City,
                    Country = a.Country,
                    Latitude = a.Latitude,
                    Longitude = a.Longitude,
                }).ToList()
            };
        }
    }
}