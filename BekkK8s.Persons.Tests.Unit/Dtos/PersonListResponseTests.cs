using System.Collections.Generic;
using BekkK8s.Persons.Api.Dtos.Persons;
using BekkK8s.Persons.Core.Entities;
using FakeItEasy;
using NUnit.Framework;

namespace BekkK8s.Persons.Tests.Unit.Dtos
{
    [TestFixture]
    public class PersonListResponseTests
    {
        [Test]
        public void TheObjectContainsTheIPersonsFromConstructor()
        {
            // Arrange
            IEnumerable<IPerson> persons = A.CollectionOfFake<IPerson>(6);

            // Act
            var result = new PersonsListResponse(persons);
            
            // Assert
            Assert.AreEqual(persons, result.Persons);
        }

        [Test]
        public void TheObjectContainsTheRightTotalAmountOfPersons()
        {
            // Arrange
            IEnumerable<IPerson> persons = A.CollectionOfFake<IPerson>(20);

            // Act
            var result = new PersonsListResponse(persons);
            
            // Assert
            Assert.AreEqual(20, result.TotalPersons);
        }
    }
}